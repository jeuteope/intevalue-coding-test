package com.teope.mini.bank.exception.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InsufficientBalanceResponse {
	private String error;
}
