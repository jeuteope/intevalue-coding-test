package com.teope.mini.bank.service.impl;

import static com.teope.mini.bank.constant.TransactionType.DEPOSIT;
import static com.teope.mini.bank.constant.TransactionType.TRANSFER_FROM;
import static com.teope.mini.bank.constant.TransactionType.TRANSFER_TO;
import static com.teope.mini.bank.constant.TransactionType.WITHDRAW;

import java.util.List;

import org.springframework.stereotype.Service;

import com.teope.mini.bank.exception.InsufficientBalanceException;
import com.teope.mini.bank.model.Account;
import com.teope.mini.bank.model.Transaction;
import com.teope.mini.bank.model.User;
import com.teope.mini.bank.pojo.TransactionForm;
import com.teope.mini.bank.repository.AccountRepository;
import com.teope.mini.bank.repository.TransactionRepository;
import com.teope.mini.bank.repository.UserRepository;
import com.teope.mini.bank.service.TransactionService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

	private final TransactionRepository transactionRepository;
	private final UserRepository userRepository;
	private final AccountRepository accountRepository;
	
	@Override
	public Transaction save(Transaction transaction) {
		if(transaction.getAccount() != null) {
			Account account = accountRepository.findById(transaction.getAccount().getId()).get();
			transaction.setAccount(account);
		}
		
		if(transaction.getUser() != null){
			User user = userRepository.findById(transaction.getUser().getId()).get();
			transaction.setUser(user);
		}
		return transactionRepository.save(transaction);
	}

	@Override
	public List<Transaction> findAllByUserId(Long id) {
		return transactionRepository.findAllByUserId(id);
	}

	@Override
	public Double getTotalBalance(Long number) {
		List<Transaction> transactions = findAllByAccountNumber(number);
		Double balance = 0.0;
		if(transactions != null) {
			for (Transaction transaction : transactions) {
				if(transaction.getTransactionType().equals(DEPOSIT) || transaction.getTransactionType().equals(TRANSFER_TO)) {
					balance += transaction.getAmount();
				} if(transaction.getTransactionType().equals(WITHDRAW) || transaction.getTransactionType().equals(TRANSFER_FROM)) {
					balance -= transaction.getAmount();
				}
			}
		}
		return balance;
	}

	@Override
	public List<Transaction> findAllByAccountNumber(Long number) {
		return transactionRepository.findAllByAccountNumber(number);
	}

	@Override
	public void processForm(TransactionForm form) {
		boolean insufficientBalanceFlag = false;
		
		if(form.getTransferFrom() != null && form.getTransferTo() != null ) {
			Double bal = getTotalBalance(form.getTransferFrom());
			if(form.getAmount() <= bal) {
				Account fromAcc = accountRepository.findByNumber(form.getTransferFrom());
				Account toAcc = accountRepository.findByNumber(form.getTransferTo());
				
				transactionRepository.save(new Transaction(fromAcc, null, TRANSFER_FROM, form.getAmount()));
				transactionRepository.save(new Transaction(toAcc, null, TRANSFER_TO, form.getAmount()));
			} else if(form.getAmount() > bal) {
				insufficientBalanceFlag = true;
			}
		} else if(form.getTransactionType() != null) {
			Account acc = accountRepository.findByNumber(form.getAccountNumber());
			User user = userRepository.findById(form.getUserId()).get();
			if(form.getTransactionType().equals(DEPOSIT)) {
				transactionRepository.save(new Transaction(acc, user, DEPOSIT, form.getAmount()));
			} else if(form.getTransactionType().equals(WITHDRAW)) {
				Double bal = getTotalBalance(form.getAccountNumber());
				if(form.getAmount() <= bal) {
					transactionRepository.save(new Transaction(acc, user, WITHDRAW, form.getAmount()));
				} else if (form.getAmount() > bal) {
					insufficientBalanceFlag = true;
				}
			}
		}
		
		if(insufficientBalanceFlag) {
			throw new InsufficientBalanceException("Insufficient balance. Please try again.");
		}
	}

}
