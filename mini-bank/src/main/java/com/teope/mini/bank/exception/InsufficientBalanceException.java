package com.teope.mini.bank.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(BAD_REQUEST)
public class InsufficientBalanceException extends RuntimeException {

	private static final long serialVersionUID = 8663671323789069594L;
	
	public InsufficientBalanceException(String message) {
		super(message);
	}

}
