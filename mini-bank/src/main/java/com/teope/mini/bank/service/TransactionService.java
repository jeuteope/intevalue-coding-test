package com.teope.mini.bank.service;

import java.util.List;

import com.teope.mini.bank.model.Transaction;
import com.teope.mini.bank.pojo.TransactionForm;

public interface TransactionService {

	public Double getTotalBalance(Long number);
	public void processForm(TransactionForm form);
	public Transaction save(Transaction transaction);
	public List<Transaction> findAllByUserId(Long id);
	public List<Transaction> findAllByAccountNumber(Long number);
}
