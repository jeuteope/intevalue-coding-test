package com.teope.mini.bank.pojo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.teope.mini.bank.constant.TransactionType;

import lombok.Data;

@Data
public class TransactionForm {

	private Long transferFrom;
	private Long transferTo;
	private Long accountNumber;
	private Long userId;
	private TransactionType transactionType;
	
	@NotNull(message = "Amount is required")
	@Min(message = "Amount should be greater than 0", value = 1l)
	private Double amount; 
}
