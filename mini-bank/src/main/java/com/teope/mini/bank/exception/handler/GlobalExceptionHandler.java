package com.teope.mini.bank.exception.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.teope.mini.bank.exception.InsufficientBalanceException;
import com.teope.mini.bank.exception.response.InsufficientBalanceResponse;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler
	protected final ResponseEntity<Object> hhandleInsufficientBalanceException(InsufficientBalanceException ex,
			WebRequest request) {
		return new ResponseEntity<>(new InsufficientBalanceResponse(ex.getMessage()), BAD_REQUEST);
	}
	
}
