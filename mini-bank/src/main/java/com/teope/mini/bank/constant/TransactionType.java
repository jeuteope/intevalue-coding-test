package com.teope.mini.bank.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransactionType {

	WITHDRAW("WITHDRAW"),
	DEPOSIT("DEPOSIT"),
	TRANSFER_FROM("TRANSFER_FROM"),
	TRANSFER_TO("TRANSFER_TO");

	private String value;
}
