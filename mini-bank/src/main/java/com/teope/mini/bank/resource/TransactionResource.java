package com.teope.mini.bank.resource;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teope.mini.bank.model.Transaction;
import com.teope.mini.bank.pojo.TransactionForm;
import com.teope.mini.bank.service.EntityValidationErrorService;
import com.teope.mini.bank.service.TransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/transactions")
@RequiredArgsConstructor
public class TransactionResource {
	
	private final TransactionService transactionService;
	private final EntityValidationErrorService entityValidationErrorService;
	
	@PostMapping("")
	public ResponseEntity<?> createOrEditTransaction(@Valid @RequestBody TransactionForm form, BindingResult result){
		if(result.hasErrors()) return entityValidationErrorService.validate(result);
		transactionService.processForm(form);	
		return new ResponseEntity<Void>(CREATED);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<?> getTransactionsByUserID(@PathVariable Long id){
		return new ResponseEntity<List<Transaction>>(transactionService.findAllByUserId(id), OK);
	}
	
	@GetMapping("/account/{number}")
	public ResponseEntity<?> getTransactionsByAccountNumber(@PathVariable Long number){
		return new ResponseEntity<List<Transaction>>(transactionService.findAllByAccountNumber(number), OK);
	}
}
