package com.teope.mini.bank.resource;

import static org.springframework.http.HttpStatus.CREATED;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teope.mini.bank.model.Account;
import com.teope.mini.bank.service.AccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/accounts")
@RequiredArgsConstructor
public class AccountResource {

	private final AccountService accountService;
	
	@PostMapping("")
	public ResponseEntity<?> createOrEditAccount(@Valid @RequestBody Account account, @RequestParam(required = false) Long id){
		return new ResponseEntity<Account>(accountService.save(account, id), CREATED);
	}
	
}
