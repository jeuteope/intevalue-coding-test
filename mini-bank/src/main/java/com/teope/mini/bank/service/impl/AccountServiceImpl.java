package com.teope.mini.bank.service.impl;

import org.springframework.stereotype.Service;

import com.teope.mini.bank.model.Account;
import com.teope.mini.bank.model.User;
import com.teope.mini.bank.repository.AccountRepository;
import com.teope.mini.bank.repository.UserRepository;
import com.teope.mini.bank.service.AccountService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;
	private final UserRepository userRepository;
	
	@Override
	public Account save(Account account, Long userId) {
		if(userId != null) {
			User user = userRepository.findById(userId).get();
			account.setUser(user);
		}
		return accountRepository.save(account);
	}

}
