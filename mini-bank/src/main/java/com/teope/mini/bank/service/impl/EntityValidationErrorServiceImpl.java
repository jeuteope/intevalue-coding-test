package com.teope.mini.bank.service.impl;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.teope.mini.bank.service.EntityValidationErrorService;

@Service
public class EntityValidationErrorServiceImpl implements EntityValidationErrorService{

	@Override
	public ResponseEntity<?> validate(BindingResult result) {
		if(result.hasErrors()) {
			Map<String, String> errorMap = new HashMap<>();
			result.getFieldErrors().forEach((e) -> {errorMap.put(e.getField(), e.getDefaultMessage());});
			return new ResponseEntity<Map<String,String>>(errorMap, BAD_REQUEST);
		}
		return null;
	}

	
}
