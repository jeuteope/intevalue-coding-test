package com.teope.mini.bank.service;

import com.teope.mini.bank.model.User;

public interface UserService {

	public Long getTotalUsers();
	public User save(User user);
}
