package com.teope.mini.bank.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import com.teope.mini.bank.model.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class Auditable<U> extends BaseEntity {

	private static final long serialVersionUID = 1945635607489176927L;

	@CreatedBy
	@OneToOne
	@JoinColumn(name = "created_by_id", updatable = false)
	protected U createdBy;

	@CreatedDate
	@DateTimeFormat
	@Column(updatable = false)
	protected LocalDateTime creationDate;

	@LastModifiedBy
	@OneToOne
	@JoinColumn(name = "last_modified_by_id")
	protected U lastModifiedBy;

	@LastModifiedDate
	@DateTimeFormat
	protected LocalDateTime lastModifiedDate;
}
