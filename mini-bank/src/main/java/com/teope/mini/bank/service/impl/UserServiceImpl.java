package com.teope.mini.bank.service.impl;

import org.springframework.stereotype.Service;

import com.teope.mini.bank.model.User;
import com.teope.mini.bank.repository.UserRepository;
import com.teope.mini.bank.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	@Override
	public Long getTotalUsers() {
		return userRepository.count();
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

}
