package com.teope.mini.bank.resource;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teope.mini.bank.model.User;
import com.teope.mini.bank.service.EntityValidationErrorService;
import com.teope.mini.bank.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UserResource {

	private final UserService userService;
	private final EntityValidationErrorService entityValidationErrorService;
	
	@PostMapping("")
	public ResponseEntity<?> createOrEditUser(@Valid @RequestBody User user, BindingResult result){
		if(result.hasErrors()) return entityValidationErrorService.validate(result);
		return new ResponseEntity<User>(userService.save(user), CREATED);
	}
	
	@GetMapping("/total")
	public ResponseEntity<?> getTotalUserCount() {
		return new ResponseEntity<Long>(userService.getTotalUsers(), OK);
	}
	
}
