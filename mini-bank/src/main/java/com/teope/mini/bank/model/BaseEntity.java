package com.teope.mini.bank.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;


@MappedSuperclass
@Data
public abstract class BaseEntity implements Serializable {
	
	private static final long serialVersionUID = -1641219240215360884L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	protected Long id;
}
