package com.teope.mini.bank.service;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface EntityValidationErrorService {
	public ResponseEntity<?> validate(BindingResult result);
}
