package com.teope.mini.bank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.teope.mini.bank.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{
	
	@Query("SELECT t FROM Transaction t" 
			+ " JOIN t.user u "
			+ " WHERE (u.id = :id)")
	public List<Transaction> findAllByUserId(@Param("id") Long id);
	
	@Query("SELECT t FROM Transaction t" 
			+ " JOIN t.account a "
			+ " WHERE (a.number = :number)")
	public List<Transaction> findAllByAccountNumber(@Param("number") Long number);
}
