package com.teope.mini.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.teope.mini.bank.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
