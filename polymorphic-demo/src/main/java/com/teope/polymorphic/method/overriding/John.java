package com.teope.polymorphic.method.overriding;

public class John extends Person {

	@Override
	public void displayInfo() {
		System.out.println("Hello I'm John");
	}

	@Override
	public void gender() {
		System.out.println("My gender is male.");
	}

}
