package com.teope.polymorphic.method.overloading;

public class Counter {
	
	public void count() {
		for(int i = 0; i < 10; i++) {
			System.out.println(i);
		}
	}
	
	public void count(int length) {
		for(int i = 0; i < length; i++) {
			System.out.println(i);
		}
	}
}
