package com.teope.polymorphic.method.overriding;

public class Jean extends Person {

	@Override
	public void displayInfo() {
		System.out.println("Hello I'm Jean");
	}

	@Override
	public void gender() {
		System.out.println("My gender is female.");
	}

}
