package com.teope.polymorphic.method.overriding;

public class Person {
	
	public void displayInfo() {
		System.out.println("I'm a person");
	}
	
	public void gender() {
		System.out.println("I'm either a male or female.");
	}
}
