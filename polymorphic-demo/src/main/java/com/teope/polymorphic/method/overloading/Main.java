package com.teope.polymorphic.method.overloading;

public class Main {

	public static void main(String[] args) {
	
		Counter c = new Counter();		
		c.count();
		System.out.println();
		c.count(20);

	}

}
