package com.teope.polymorphic.method.overriding;

public class Main {

	public static void main(String[] args) {
		
		Person person = new Person();
		person.displayInfo();
		person.gender();
		
		John john = new John();
		john.displayInfo();
		john.gender();
		
		Jean jean = new Jean();
		jean.displayInfo();
		jean.gender();
	}

}
