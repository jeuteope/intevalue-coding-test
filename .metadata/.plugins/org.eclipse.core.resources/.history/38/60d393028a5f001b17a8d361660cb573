package com.teope.mini.bank.model;

import static javax.persistence.CascadeType.ALL;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Account extends Auditable<User> {
	
	private static final long serialVersionUID = 1601294649977389106L;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
	@OneToMany(mappedBy = "account", cascade = ALL, orphanRemoval = true)
	private List<Account> accounts = new ArrayList<>();
}
