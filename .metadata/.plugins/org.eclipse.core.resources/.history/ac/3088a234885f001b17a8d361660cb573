package com.teope.mini.bank.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class User extends Auditable<User> {

	private static final long serialVersionUID = -3393947295220796611L;
	private String firstName;
	private String lastName;

	@NotBlank(message = "Username is required")
	@Email(message = "Email must be valid")
	private String email;
	
	@NotBlank(message = "Username is required")
	@Column(updatable = false)
	private String username;
	
	private List<Account> accounts = new ArrayList<>();
}
