package com.teope.mini.bank.model;

import static javax.persistence.CascadeType.ALL;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class User extends Auditable<User> {
	
	private static final long serialVersionUID = -8059792330355657402L;
	
	private String firstName;
	private String lastName;

	@Email(message = "Email must be valid")
	private String email;
	
	@NotBlank(message = "Username is required")
	@Column(updatable = false, unique = true)
	private String username;
	
	@OneToMany(mappedBy = "user", cascade = ALL, orphanRemoval = true)
	private List<Account> accounts = new ArrayList<>();
}
