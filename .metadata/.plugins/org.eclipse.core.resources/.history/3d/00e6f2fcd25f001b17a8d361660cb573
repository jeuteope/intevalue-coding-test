package com.teope.mini.bank.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.teope.mini.bank.constant.TransactionType;
import com.teope.mini.bank.model.Account;
import com.teope.mini.bank.model.Transaction;
import com.teope.mini.bank.model.User;
import com.teope.mini.bank.repository.AccountRepository;
import com.teope.mini.bank.repository.TransactionRepository;
import com.teope.mini.bank.repository.UserRepository;
import com.teope.mini.bank.service.TransactionService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

	private final TransactionRepository transactionRepository;
	private final UserRepository UserRepository;
	private final AccountRepository AccountRepository;
	
	@Override
	public Transaction save(Transaction transaction) {
		if(transaction.getAccount() != null) {
			Account account = AccountRepository.findById(transaction.getAccount().getId()).get();
			transaction.setAccount(account);
		}
		
		if(transaction.getUser() != null){
			User user = UserRepository.findById(transaction.getUser().getId()).get();
			transaction.setUser(user);
		}
		return transactionRepository.save(transaction);
	}

	@Override
	public List<Transaction> findAllByUserId(Long id) {
		return transactionRepository.findAllByUserId(id);
	}

	@Override
	public Double getTotalBalance(Long number) {
		List<Transaction> transactions = transactionRepository.findAllByAccountNumber(number);
		Double balance = 0.0;
		if(transactions != null) {
			transactions.forEach((t) -> {
				if(t.getTransactionType().equals(TransactionType.DEPOSIT)) {
					balance += t.getAmount();
				}
			});
		}
		return balance;
	}

}
