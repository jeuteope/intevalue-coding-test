package com.teope.mini.bank.model;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.teope.mini.bank.constant.TransactionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Data 
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends Auditable<User> {
	
	private static final long serialVersionUID = -8198974060158272947L;

	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@NotBlank(message = "Transaction Type is required")
	@Enumerated(STRING)
	private TransactionType transactionType;
	
	@NotBlank(message = "Amount is required")
	@Min(message = "Amount must be greater than zero", value = 0)
	private Double amount;
	
	
}
