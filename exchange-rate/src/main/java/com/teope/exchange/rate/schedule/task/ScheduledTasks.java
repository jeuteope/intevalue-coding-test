package com.teope.exchange.rate.schedule.task;

import static java.time.temporal.ChronoUnit.DAYS;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.teope.exchange.rate.model.ExchangeRate;
import com.teope.exchange.rate.repository.ExchangeRateRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {

	private final RestTemplate restTemplate;
	private final ExchangeRateRepository exchangeRateRepository;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

//	@Scheduled(cron = "0 0 0 * * *") -- Daily
	@Scheduled(fixedRate = 10000)
	public void getFixedExchangeRateSystemForPHP() {
		ExchangeRate er = restTemplate.getForObject("https://api.exchangeratesapi.io/latest?base=PHP",
				ExchangeRate.class);
		exchangeRateRepository.save(er);
	}

	//EUR
	@Scheduled(fixedRate = 10000)
	public void getExchangeRateTwoDaysAgo() {
		String twoDaysAgo = formatter.format(Date.from(Instant.now().minus(2, DAYS)));
		
		ExchangeRate er = restTemplate.getForObject("https://api.exchangeratesapi.io/" + twoDaysAgo, ExchangeRate.class);
		exchangeRateRepository.save(er);
	}
	
	@Scheduled(fixedRate = 10000)
	public void getFixedExchangeRateSystemForUSD() {
		ExchangeRate er = restTemplate.getForObject("https://api.exchangeratesapi.io/latest?base=USD",
				ExchangeRate.class);
		exchangeRateRepository.save(er);
	}
}
