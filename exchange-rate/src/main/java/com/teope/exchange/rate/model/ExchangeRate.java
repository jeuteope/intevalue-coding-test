package com.teope.exchange.rate.model;

import static javax.persistence.CascadeType.ALL;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class ExchangeRate extends BaseEntity {
	
	private static final long serialVersionUID = 2896192725771014133L;
	
	@OneToOne(cascade = ALL)
	@JoinColumn(name = "rates_id")
	private Rates rates;
	
	private String base;
	private Date date;
}
